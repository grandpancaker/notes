﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NotesWeb.Models;
using System.Diagnostics;

namespace NotesWeb.Controllers
{
    [Authorize]
    public class BrowseController : Controller
    {
        private NotesContext db = new NotesContext();

        // GET: Browse

        public ActionResult Index(bool filt = false)
        {
            if(filt)
            {
                var list = TempData["notes"] as List<Note>;
                ViewBag.Message = "Filter results:";
                return View(list);
            }
            else
            {
                ViewBag.Message = "";
                return View(db.Notes.ToList());
            }
        }

        // GET: Browse/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Note note = db.Notes.Include(n => n.Attachments).SingleOrDefault(n => n.NoteId == id);
            if (note == null)
            {
                return HttpNotFound();
            }
            return View(note);
        }

        public ActionResult GetImg(int id)
        {
            Attachment at = db.Attachments.Find(id);
            return File(at.Picture, "image/jpg");
        }

        public ActionResult Filter()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Filter(Category c)
        {
            if (ModelState.IsValid)
            {
                Importance imp = c.Importance;
                ClassType ct = c.ClassType;
                var list = db.Notes.Where(n => (n.Category.Importance == imp) && (n.Category.ClassType == ct)).ToList();
                TempData["notes"] = list;
                return RedirectToAction("Index", new { filt = true });
            }

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
