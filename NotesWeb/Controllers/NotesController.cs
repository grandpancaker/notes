﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NotesWeb.Models;
using System.Diagnostics;
using Microsoft.AspNet.Identity;
using NotesWeb.Services.DataBaseServices;
using NotesWeb.Services.InterfaceService;
using System.IO;

namespace NotesWeb.Controllers
{
    [Authorize]
    public class NotesController : Controller
    {
        private NotesContext db = new NotesContext();
        private INoteService _noteService = new NoteDbService();
        

        // GET: Notes
        public ActionResult Index(bool filt = false)
        {
            if (filt)
            {
                var list = TempData["notes"] as List<Note>;
                ViewBag.Message = "Filter results:";
                return View(list);
            }
            else
            {
                var noteList = _noteService.SelectAllForUser(User.Identity.GetUserId());
                ViewBag.Message = "";
                return View(noteList);
            }
        }

        // GET: Notes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Note note = db.Notes.Include(n => n.Attachments).SingleOrDefault(n => n.NoteId == id);
            if (note == null)
            {
                return HttpNotFound();
            }
            return View(note);
        }

        // GET: Notes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Notes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "NoteId,Name,Category,Text,CreationDate,UpdateDate")] Note note)
        {
            if (ModelState.IsValid)
            {
                var user = db.Users.Find(User.Identity.GetUserId()) as User;
                note.Users = new List<User>();
                note.Users.Add(user);
                note.CreationDate = DateTime.Now;
                note.UpdateDate = DateTime.Now;
                db.Notes.Add(note);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(note);
        }

        public ActionResult Upload(int id)
        {
            Note note = db.Notes.Find(id);
            return View(note);
        }

        [HttpPost]
        public ActionResult Upload(int id,HttpPostedFileBase upload)
        {
            if (upload != null && upload.ContentLength > 0)
            {
                var at = new Attachment();
                using (var reader = new System.IO.BinaryReader(upload.InputStream))
                {
                    at.Picture = reader.ReadBytes(upload.ContentLength);
                }
                var note = db.Notes.Find(id);
                if (note.Attachments == null) note.Attachments = new List<Attachment>();
                note.Attachments.Add(at);
                note.UpdateDate = DateTime.Now;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult GetImg(int id)
        {
            Attachment at = db.Attachments.Find(id);
            return File(at.Picture, "image/jpg");
        }

        // GET: Notes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Note note = db.Notes.Find(id);
            if (note == null)
            {
                return HttpNotFound();
            }
            return View(note);
        }

        // POST: Notes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "NoteId,Name,Category,Text,CreationDate")] Note note)
        {
            if (ModelState.IsValid)
            {
                note.UpdateDate = DateTime.Now;
                db.Entry(note).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(note);
        }

        // GET: Notes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Note note = db.Notes.Find(id);
            if (note == null)
            {
                return HttpNotFound();
            }
            return View(note);
        }

        // POST: Notes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _noteService.Delete(id);
            //Note note = db.Notes.Find(id);
            //db.Notes.Remove(note);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Filter()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Filter(Category c)
        {
            if (ModelState.IsValid)
            {
                Importance imp = c.Importance;
                ClassType ct = c.ClassType;
                var noteList = _noteService.SelectAllForUser(User.Identity.GetUserId());
                var list = noteList.Where(n => (n.Category.Importance == imp) && (n.Category.ClassType == ct)).ToList();
                TempData["notes"] = list;
                return RedirectToAction("Index", new { filt = true });
            }

            return View();
        }
    }
}
