﻿using System.ComponentModel.DataAnnotations;
using System.IO;
//using System.Windows.Controls;
using System.Drawing.Imaging;
using Newtonsoft.Json;

namespace NotesWeb.Models
{
    public class Attachment
    {
        [Key] public int AttachmentId { get; set; }
        public byte[] Picture { get; set; } //stores png
        [JsonIgnore]
        public virtual Note Note { get; set; }

        public System.Drawing.Image GetPicture()
        {
            MemoryStream ms = new MemoryStream(Picture);
            System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
            return returnImage;
        }

        public void SavePicture(System.Drawing.Image imageIn)
        {
            Picture = ImageToByteArray(imageIn);
        }
    
        private byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, ImageFormat.Png);
                return ms.ToArray();
            }
        }

      
    }
}