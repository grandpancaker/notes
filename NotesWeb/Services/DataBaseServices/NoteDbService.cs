using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using NotesWeb.Models;
using NotesWeb.Services.InterfaceService;

namespace NotesWeb.Services.DataBaseServices
{
    public class NoteDbService : INoteService
    {
        private NotesContext dbContext = new NotesContext();
        public bool Create(Note note)
        {
            using (var dbContext = new NotesContext())
            {
                try
                {
                    dbContext.Notes.Add(note);
                    dbContext.SaveChanges();
                     
                    return true;
                }
                catch (Exception e)
                {
                       
                    return false;
                }
            }
        }

        public bool Update(Note note)
        {
            using (var dbContext = new NotesContext())
            {
                dbContext.Entry(note).State = EntityState.Modified;
                return dbContext.SaveChanges() > 0;
            }
        }

        public Note Select(int id)
        {
            
                return dbContext.Notes.Find(id);
            
        }

        public List<Note> SelectAll()
        {
           
                return dbContext.Notes.ToList();
            
        }

        public List<Note> SelectAllForUser(string id)
        {
            
                return dbContext.Notes.Where(n=> n.Users.Any(u=>u.Id == id)).ToList();
            
        }

        public bool Delete(int id)
        {           
                var attachmentService = new AttachmentDbService();
                var note = dbContext.Notes.Find(id);
                attachmentService.DeleteByNote(id);
                note.Attachments = null;
                dbContext.Notes.Remove(dbContext.Notes.Find(id));
                return dbContext.SaveChanges() > 0;            
        }
    }
}