﻿using System.Collections.Generic;
using NotesWeb.Models;

namespace NotesWeb.Services.InterfaceService
{
    public interface INoteService
    {
        bool Create(Note note);
        bool Update(Note note);
        Note Select(int id);
        List<Note> SelectAll();
        List<Note> SelectAllForUser(string id);
        bool Delete(int id);
    }
}